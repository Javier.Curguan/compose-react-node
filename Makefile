DETACHED:=-d
ACCESS_KEY_S3:=""
AWS_SECRET:=""

vars:
	@echo "AWS_SECRET:      "$(AWS_SECRET)
	@echo "ACCESS_KEY_S3:    "$(ACCESS_KEY_S3)

project-up: stop-all 
	docker-compose up $(DETACHED)  --remove-orphans  mongodb backend-node frontend-ionic  nginx-proxy
stop-all: 
	docker-compose stop
project-node-install:
	docker-compose exec backend-node npm install 
project-exec-node-back:
	docker-compose exec backend-node /bin/bash
project-ionic-install:
	docker-compose exec frontend-ionic npm install 
project-build-ionic-front: project-ionic-install
	docker-compose exec frontend-ionic ionic build
project-exec-ionic-front:
	docker-compose exec frontend-ionic /bin/bash
# oucpado perl para asegurar funcionamiento en cualquier distro linux
init-minio-config:
	cp ./minio/config_example.json ./minio/config.json 
	LANG=C perl  -i -pe's/ACCESS_KEY_S3/$(ACCESS_KEY_S3)/g' ./minio/config.json 
	LANG=C perl  -i -pe's/SECRET_KEY_S3/$(AWS_SECRET)/g' ./minio/config.json
publish-static:
	docker-compose up  publish-static
ssl-create:
	docker-compose up  ssl-lego
